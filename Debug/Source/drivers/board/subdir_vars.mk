################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Source/drivers/board/BatteryGas.c \
../Source/drivers/board/BatteryGasI2C.c \
../Source/drivers/board/accel.c \
../Source/drivers/board/ads1220.c \
../Source/drivers/board/board_gpio.c \
../Source/drivers/board/compass.c \
../Source/drivers/board/ext_interface.c \
../Source/drivers/board/gyro_st.c \
../Source/drivers/board/rtc.c \
../Source/drivers/board/ser_number.c \
../Source/drivers/board/ti_radio_module.c 

C_DEPS += \
./Source/drivers/board/BatteryGas.d \
./Source/drivers/board/BatteryGasI2C.d \
./Source/drivers/board/accel.d \
./Source/drivers/board/ads1220.d \
./Source/drivers/board/board_gpio.d \
./Source/drivers/board/compass.d \
./Source/drivers/board/ext_interface.d \
./Source/drivers/board/gyro_st.d \
./Source/drivers/board/rtc.d \
./Source/drivers/board/ser_number.d \
./Source/drivers/board/ti_radio_module.d 

OBJS += \
./Source/drivers/board/BatteryGas.obj \
./Source/drivers/board/BatteryGasI2C.obj \
./Source/drivers/board/accel.obj \
./Source/drivers/board/ads1220.obj \
./Source/drivers/board/board_gpio.obj \
./Source/drivers/board/compass.obj \
./Source/drivers/board/ext_interface.obj \
./Source/drivers/board/gyro_st.obj \
./Source/drivers/board/rtc.obj \
./Source/drivers/board/ser_number.obj \
./Source/drivers/board/ti_radio_module.obj 

OBJS__QUOTED += \
"Source\drivers\board\BatteryGas.obj" \
"Source\drivers\board\BatteryGasI2C.obj" \
"Source\drivers\board\accel.obj" \
"Source\drivers\board\ads1220.obj" \
"Source\drivers\board\board_gpio.obj" \
"Source\drivers\board\compass.obj" \
"Source\drivers\board\ext_interface.obj" \
"Source\drivers\board\gyro_st.obj" \
"Source\drivers\board\rtc.obj" \
"Source\drivers\board\ser_number.obj" \
"Source\drivers\board\ti_radio_module.obj" 

C_DEPS__QUOTED += \
"Source\drivers\board\BatteryGas.d" \
"Source\drivers\board\BatteryGasI2C.d" \
"Source\drivers\board\accel.d" \
"Source\drivers\board\ads1220.d" \
"Source\drivers\board\board_gpio.d" \
"Source\drivers\board\compass.d" \
"Source\drivers\board\ext_interface.d" \
"Source\drivers\board\gyro_st.d" \
"Source\drivers\board\rtc.d" \
"Source\drivers\board\ser_number.d" \
"Source\drivers\board\ti_radio_module.d" 

C_SRCS__QUOTED += \
"../Source/drivers/board/BatteryGas.c" \
"../Source/drivers/board/BatteryGasI2C.c" \
"../Source/drivers/board/accel.c" \
"../Source/drivers/board/ads1220.c" \
"../Source/drivers/board/board_gpio.c" \
"../Source/drivers/board/compass.c" \
"../Source/drivers/board/ext_interface.c" \
"../Source/drivers/board/gyro_st.c" \
"../Source/drivers/board/rtc.c" \
"../Source/drivers/board/ser_number.c" \
"../Source/drivers/board/ti_radio_module.c" 


