################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Source/drivers/DataManager.c \
../Source/drivers/WTTTSProtocolUtils.c \
../Source/drivers/utils.c 

C_DEPS += \
./Source/drivers/DataManager.d \
./Source/drivers/WTTTSProtocolUtils.d \
./Source/drivers/utils.d 

OBJS += \
./Source/drivers/DataManager.obj \
./Source/drivers/WTTTSProtocolUtils.obj \
./Source/drivers/utils.obj 

OBJS__QUOTED += \
"Source\drivers\DataManager.obj" \
"Source\drivers\WTTTSProtocolUtils.obj" \
"Source\drivers\utils.obj" 

C_DEPS__QUOTED += \
"Source\drivers\DataManager.d" \
"Source\drivers\WTTTSProtocolUtils.d" \
"Source\drivers\utils.d" 

C_SRCS__QUOTED += \
"../Source/drivers/DataManager.c" \
"../Source/drivers/WTTTSProtocolUtils.c" \
"../Source/drivers/utils.c" 


