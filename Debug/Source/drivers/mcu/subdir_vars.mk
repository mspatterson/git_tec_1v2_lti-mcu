################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Source/drivers/mcu/adc12.c \
../Source/drivers/mcu/flash.c \
../Source/drivers/mcu/gpio.c \
../Source/drivers/mcu/i2c.c \
../Source/drivers/mcu/spi.c \
../Source/drivers/mcu/system.c \
../Source/drivers/mcu/timer.c \
../Source/drivers/mcu/uart.c 

C_DEPS += \
./Source/drivers/mcu/adc12.d \
./Source/drivers/mcu/flash.d \
./Source/drivers/mcu/gpio.d \
./Source/drivers/mcu/i2c.d \
./Source/drivers/mcu/spi.d \
./Source/drivers/mcu/system.d \
./Source/drivers/mcu/timer.d \
./Source/drivers/mcu/uart.d 

OBJS += \
./Source/drivers/mcu/adc12.obj \
./Source/drivers/mcu/flash.obj \
./Source/drivers/mcu/gpio.obj \
./Source/drivers/mcu/i2c.obj \
./Source/drivers/mcu/spi.obj \
./Source/drivers/mcu/system.obj \
./Source/drivers/mcu/timer.obj \
./Source/drivers/mcu/uart.obj 

OBJS__QUOTED += \
"Source\drivers\mcu\adc12.obj" \
"Source\drivers\mcu\flash.obj" \
"Source\drivers\mcu\gpio.obj" \
"Source\drivers\mcu\i2c.obj" \
"Source\drivers\mcu\spi.obj" \
"Source\drivers\mcu\system.obj" \
"Source\drivers\mcu\timer.obj" \
"Source\drivers\mcu\uart.obj" 

C_DEPS__QUOTED += \
"Source\drivers\mcu\adc12.d" \
"Source\drivers\mcu\flash.d" \
"Source\drivers\mcu\gpio.d" \
"Source\drivers\mcu\i2c.d" \
"Source\drivers\mcu\spi.d" \
"Source\drivers\mcu\system.d" \
"Source\drivers\mcu\timer.d" \
"Source\drivers\mcu\uart.d" 

C_SRCS__QUOTED += \
"../Source/drivers/mcu/adc12.c" \
"../Source/drivers/mcu/flash.c" \
"../Source/drivers/mcu/gpio.c" \
"../Source/drivers/mcu/i2c.c" \
"../Source/drivers/mcu/spi.c" \
"../Source/drivers/mcu/system.c" \
"../Source/drivers/mcu/timer.c" \
"../Source/drivers/mcu/uart.c" 


