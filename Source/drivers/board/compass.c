/*
 * compass.c
 *
 *  Created on: 2012-07-25
 *      Author: DD
 */
 
#include "compass.h"

#define COMPASS_BUF_SIZE 20

I2C_OBJECT	i2cCompass; 
BYTE RxCompassBuffer[COMPASS_BUF_SIZE];
BYTE TxCompassBuffer[COMPASS_BUF_SIZE];
TIMERHANDLE thCompassTimer;

const BYTE CompassDataRate[]=
{
		COMP_M_ODR_800HZ,
		COMP_M_ODR_400HZ,
		COMP_M_ODR_200HZ,
		COMP_M_ODR_100HZ,
		COMP_M_ODR_50HZ,
		COMP_M_ODR_12HZ,
		COMP_M_ODR_6HZ,
		COMP_M_ODR_1HZ

};
int	CompassData[3];
 
//void CompassInit(BYTE number)
//{
//	BYTE i=0;
//
//	if(thCompassTimer ==0)
//	{
//		thCompassTimer = RegisterTimer();
//	}
//
//	ResetTimer( thCompassTimer, COMPASS_POWER_UP_TIME );
//	StartTimer( thCompassTimer );
////	if(thCompassTimer)
////	{
////		while(!TimerExpired( thCompassTimer));
////	}
////	else
////	{
////		__delay_cycles(400000);
////	}
////	UnregisterTimer(thCompassTimer);
//
//	__delay_cycles(1000);
//	// clear the buffers
//	for(i=0;i<sizeof(RxCompassBuffer);i++)
//	{
//		RxCompassBuffer[i]=0;
//		TxCompassBuffer[i]=0;
//	}
//
//	switch(number)
//	{
//		case 0:
//			i2cCompass.bySlaveAddress = COMPASS_ADDRESS_1;
//			break;
//		case 1:
//			i2cCompass.bySlaveAddress = COMPASS_ADDRESS_2;
//			break;
//		default:
//			break;
//	}
//
// 	i2cCompass.ptrTxQ = TxCompassBuffer;
//   	i2cCompass.wTxQIndex = 0;
//    i2cCompass.wTxQSize = 2;
//    i2cCompass.bTxQSending = TRUE;
//    i2cCompass.ptrRxQ = RxCompassBuffer;
//    i2cCompass.wRxQIndex = 0;
//    i2cCompass.wRxQSize = 0;
//    i2cCompass.bRxQReceiving = FALSE;
//
//
//    TxCompassBuffer[0] = M_CTRL_REG1_ADDR;
//	TxCompassBuffer[1] = MAGN_ONLY_ON;
//
//    i2cCompassWrite(&i2cCompass);
////	ResetTimer( thCompassTimer, COMPASS_WRITE_TIME );
////	while(!TimerExpired( thCompassTimer));
//    __delay_cycles(10000);
//	i2cCompass.ptrTxQ = TxCompassBuffer;
//	i2cCompass.wTxQIndex = 0;
//	i2cCompass.wTxQSize = 2;
//	i2cCompass.bTxQSending = TRUE;
//	i2cCompass.ptrRxQ = RxCompassBuffer;
//	i2cCompass.wRxQIndex = 0;
//	i2cCompass.wRxQSize = 0;
//	i2cCompass.bRxQReceiving = FALSE;
//
//
//	TxCompassBuffer[0] = CTRL_REG1_ADDR;
//	TxCompassBuffer[1] = M_ODR_100HZ | ACTIVE_MODE;
//    i2cCompassWrite(&i2cCompass);
////	ResetTimer( thCompassTimer, COMPASS_WRITE_TIME );
////	while(!TimerExpired( thCompassTimer));
//    __delay_cycles(10000);
//
//}

void CompassInit(void)
{
	BYTE i=0;

	if(thCompassTimer ==0)
	{
		thCompassTimer = RegisterTimer();
	}

	ResetTimer( thCompassTimer, COMPASS_POWER_UP_TIME );
	StartTimer( thCompassTimer ); 
//	if(thCompassTimer)
//	{
//		while(!TimerExpired( thCompassTimer));
//	}
//	else
//	{
//		__delay_cycles(400000);
//	}
//	UnregisterTimer(thCompassTimer);

	__delay_cycles(400000);
	// clear the buffers
	for(i=0;i<sizeof(RxCompassBuffer);i++)
	{
		RxCompassBuffer[i]=0;
		TxCompassBuffer[i]=0;
	}
	

	i2cCompass.bySlaveAddress = COMPASS_ADDRESS_1;
	
 	i2cCompass.ptrTxQ = TxCompassBuffer;                           
   	i2cCompass.wTxQIndex = 0;                              
    i2cCompass.wTxQSize = 4;
    i2cCompass.bTxQSending = TRUE;
    i2cCompass.ptrRxQ = RxCompassBuffer;
    i2cCompass.wRxQIndex = 0;                              
    i2cCompass.wRxQSize = 0;
    i2cCompass.bRxQReceiving = FALSE;
    

    TxCompassBuffer[0] = COMP_M_CTRL_REG1_ADDR;
	TxCompassBuffer[1] = COMP_MAGN_ONLY_ON;
	TxCompassBuffer[2] = 0x10;
	TxCompassBuffer[3] = 0x80;

    i2cCompassWrite(&i2cCompass);
//	ResetTimer( thCompassTimer, COMPASS_WRITE_TIME );
//	while(!TimerExpired( thCompassTimer));
    __delay_cycles(20000);
	i2cCompass.ptrTxQ = TxCompassBuffer;
	i2cCompass.wTxQIndex = 0;
	i2cCompass.wTxQSize = 2;
	i2cCompass.bTxQSending = TRUE;
	i2cCompass.ptrRxQ = RxCompassBuffer;
	i2cCompass.wRxQIndex = 0;
	i2cCompass.wRxQSize = 0;
	i2cCompass.bRxQReceiving = FALSE;


	TxCompassBuffer[0] = COMP_CTRL_REG1_ADDR;
	TxCompassBuffer[1] = COMP_M_ODR_100HZ | COMP_ACTIVE_MODE;
    i2cCompassWrite(&i2cCompass);
//	ResetTimer( thCompassTimer, COMPASS_WRITE_TIME );
//	while(!TimerExpired( thCompassTimer));
    __delay_cycles(20000);

}

void CompassReadDataStart(void)
{

	i2cCompass.bySlaveAddress = COMPASS_ADDRESS_1;
 	i2cCompass.ptrTxQ = TxCompassBuffer;                           
   	i2cCompass.wTxQIndex = 0;                              
    i2cCompass.wTxQSize = 1;
    i2cCompass.bTxQSending = TRUE;
    i2cCompass.ptrRxQ = RxCompassBuffer;
    i2cCompass.wRxQIndex = 0;                              
    i2cCompass.wRxQSize = 6;
    i2cCompass.bRxQReceiving = TRUE;
    
    TxCompassBuffer[0] = COMP_M_OUT_X_MSB_ADDR;
//    SetOutputPin(TP2, TRUE);
    i2cCompassRead(&i2cCompass);
     	
}


BOOL CompassDataReady(void)
{
	return (!i2cCompass.bRxQReceiving);
}

BOOL CompassTxDone(void)
{
	return (!i2cCompass.bTxQSending);
}

// read all conffiguration registers
//CRA_REG_M (00h), CRB_REG_M (01h), MR_REG_M (02h)
void CompassRegisterDump(void)
{

	i2cCompass.bySlaveAddress = COMPASS_ADDRESS_1;
	i2cCompass.ptrTxQ = TxCompassBuffer;
   	i2cCompass.wTxQIndex = 0;                              
    i2cCompass.wTxQSize = 1;
    i2cCompass.bTxQSending = TRUE;
    i2cCompass.ptrRxQ = RxCompassBuffer;
    i2cCompass.wRxQIndex = 0;                              
    i2cCompass.wRxQSize = 3;
    i2cCompass.bRxQReceiving = TRUE;
    
    TxCompassBuffer[0] = COMP_M_CTRL_REG1_ADDR;		// address of the first config register
    i2cCompassRead(&i2cCompass);
}


void CompassReadId(void)
{

	i2cCompass.bySlaveAddress = COMPASS_ADDRESS_1;
	i2cCompass.ptrTxQ = TxCompassBuffer;
   	i2cCompass.wTxQIndex = 0;
    i2cCompass.wTxQSize = 1;
    i2cCompass.bTxQSending = TRUE;
    i2cCompass.ptrRxQ = RxCompassBuffer;
    i2cCompass.wRxQIndex = 0;
    i2cCompass.wRxQSize = 2;
    i2cCompass.bRxQReceiving = TRUE;

    TxCompassBuffer[0] = COMP_WHO_AM_I_ADDR;		// address of the first config register
    i2cCompassRead(&i2cCompass);
}

// copy the compass data into the given buffer 
void CompassGetRawData(BYTE *pData, WORD maxSize)
{
	unsigned int i=0;
	while((i< 6)&&(i<maxSize))
	{
		pData[i] = RxCompassBuffer[i];
		i++;
	}
}

// return the address of the compass data
BYTE* CompassGetDataPointer(void)
{
	return RxCompassBuffer;
}


int* CompassGetDataPointerInt(void)
{
	return (int*)RxCompassBuffer;
}


void CompassDisable(void)
{

	i2cCompass.bySlaveAddress = COMPASS_ADDRESS_1;
	i2cCompass.ptrTxQ = TxCompassBuffer;
	i2cCompass.wTxQIndex = 0;
	i2cCompass.wTxQSize = 2;
	i2cCompass.bTxQSending = TRUE;
	i2cCompass.ptrRxQ = RxCompassBuffer;
	i2cCompass.wRxQIndex = 0;
	i2cCompass.wRxQSize = 0;
	i2cCompass.bRxQReceiving = FALSE;

	TxCompassBuffer[0] = COMP_CTRL_REG1_ADDR;
	TxCompassBuffer[1] = 0;
    i2cCompassWrite(&i2cCompass);
    __delay_cycles(10000);
}

void CompassEnable(void)
{

	i2cCompass.bySlaveAddress = COMPASS_ADDRESS_1;
	i2cCompass.ptrTxQ = TxCompassBuffer;
	i2cCompass.wTxQIndex = 0;
	i2cCompass.wTxQSize = 2;
	i2cCompass.bTxQSending = TRUE;
	i2cCompass.ptrRxQ = RxCompassBuffer;
	i2cCompass.wRxQIndex = 0;
	i2cCompass.wRxQSize = 0;
	i2cCompass.bRxQReceiving = FALSE;


	TxCompassBuffer[0] = COMP_CTRL_REG1_ADDR;
	TxCompassBuffer[1] = COMP_M_ODR_100HZ | COMP_ACTIVE_MODE;
    i2cCompassWrite(&i2cCompass);
    __delay_cycles(10000);

}


void CompassPowerDown(void)
{

	i2cCompass.bySlaveAddress = COMPASS_ADDRESS_1;
	i2cCompass.ptrTxQ = TxCompassBuffer;
	i2cCompass.wTxQIndex = 0;
	i2cCompass.wTxQSize = 2;
	i2cCompass.bTxQSending = TRUE;
	i2cCompass.ptrRxQ = RxCompassBuffer;
	i2cCompass.wRxQIndex = 0;
	i2cCompass.wRxQSize = 0;
	i2cCompass.bRxQReceiving = FALSE;

	TxCompassBuffer[0] = COMP_CTRL_REG2_ADDR;
	TxCompassBuffer[1] = COMP_SLEEP_MODE;
    i2cCompassWrite(&i2cCompass);
    __delay_cycles(10000);

}



//******************************************************************************
//
//  Function: AccelAndCompassInit
//
//  Arguments: none
//
//  Returns: WORD Error code or OK
//
//  Description:    This function configures the FXOS8700 into 200 Hz hybrid mode,
//              meaning that both accelerometer and magnetometer data are provided at the 200 Hz rate.
//
//
//******************************************************************************
WORD AccelAndCompassInit(void)
{
    BYTE i=0;

    if(thCompassTimer ==0)
    {
        thCompassTimer = RegisterTimer();
    }

    ResetTimer( thCompassTimer, COMPASS_POWER_UP_TIME );
    StartTimer( thCompassTimer );
    __delay_cycles(400000);
    // clear the buffers
//    for(i=0;i<sizeof(RxCompassBuffer);i++)
//    {
//        RxCompassBuffer[i]=0;
//        TxCompassBuffer[i]=0;
//    }

    memset(RxCompassBuffer, 0, sizeof(RxCompassBuffer));
    memset(TxCompassBuffer, 0, sizeof(TxCompassBuffer));

    CompassReadId();
    while(CompassDataReady()==FALSE);
    // add check for ID


    // write 0000 0000 = 0x00 to accelerometer control register 1 to place FXOS8700 into standby
    // [7-1] = 0000 000
    // [0]: active=0
    i2cCompass.ptrTxQ = TxCompassBuffer;
    i2cCompass.wTxQIndex = 0;
    i2cCompass.wTxQSize = 2;
    i2cCompass.bTxQSending = TRUE;
    i2cCompass.ptrRxQ = RxCompassBuffer;
    i2cCompass.wRxQIndex = 0;
    i2cCompass.wRxQSize = 0;
    i2cCompass.bRxQReceiving = FALSE;

    TxCompassBuffer[0] = COMP_CTRL_REG1_ADDR;
    TxCompassBuffer[1] = 0x00;
    i2cCompassWrite(&i2cCompass);
    __delay_cycles(20000);

    // write 0001 1111 = 0x1F to magnetometer control register 1
    // [7]: m_acal=0: auto calibration disabled
    // [6]: m_rst=0: no one-shot magnetic reset
    // [5]: m_ost=0: no one-shot magnetic measurement
    // [4-2]: m_os=111=7: 8x oversampling (for 200Hz) to reduce magnetometer noise
    // [1-0]: m_hms=11=3: select hybrid mode with accel and magnetometer active
    i2cCompass.bySlaveAddress = COMPASS_ADDRESS_1;
    i2cCompass.ptrTxQ = TxCompassBuffer;
    i2cCompass.wTxQIndex = 0;
    i2cCompass.wTxQSize = 2;
    i2cCompass.bTxQSending = TRUE;
    i2cCompass.ptrRxQ = RxCompassBuffer;
    i2cCompass.wRxQIndex = 0;
    i2cCompass.wRxQSize = 0;
    i2cCompass.bRxQReceiving = FALSE;

    TxCompassBuffer[0] = COMP_M_CTRL_REG1_ADDR;
    TxCompassBuffer[1] = 0x1F;
    i2cCompassWrite(&i2cCompass);
    __delay_cycles(20000);

    // write 0010 0000 = 0x20 to magnetometer control register 2
    // [7]: reserved
    // [6]: reserved
    // [5]: hyb_autoinc_mode=1 to map the magnetometer registers to follow the
    // accelerometer registers
    // [4]: m_maxmin_dis=0 to retain default min/max latching even though not used
    // [3]: m_maxmin_dis_ths=0
    // [2]: m_maxmin_rst=0
    // [1-0]: m_rst_cnt=00 to enable magnetic reset each cycle
    i2cCompass.bySlaveAddress = COMPASS_ADDRESS_1;
    i2cCompass.ptrTxQ = TxCompassBuffer;
    i2cCompass.wTxQIndex = 0;
    i2cCompass.wTxQSize = 2;
    i2cCompass.bTxQSending = TRUE;
    i2cCompass.ptrRxQ = RxCompassBuffer;
    i2cCompass.wRxQIndex = 0;
    i2cCompass.wRxQSize = 0;
    i2cCompass.bRxQReceiving = FALSE;

    TxCompassBuffer[0] = COMP_M_CTRL_REG2_ADDR;
    TxCompassBuffer[1] = 0x20;
    i2cCompassWrite(&i2cCompass);
    __delay_cycles(20000);


    // write 0000 0001= 0x01 to XYZ_DATA_CFG register
    // [7]: reserved
    // [6]: reserved
    // [5]: reserved
    // [4]: hpf_out=0
    // [3]: reserved
    // [2]: reserved
    // [1-0]: fs=01 for accelerometer range of +/-4g range with 0.488mg/LSB
    i2cCompass.bySlaveAddress = COMPASS_ADDRESS_1;
    i2cCompass.ptrTxQ = TxCompassBuffer;
    i2cCompass.wTxQIndex = 0;
    i2cCompass.wTxQSize = 2;
    i2cCompass.bTxQSending = TRUE;
    i2cCompass.ptrRxQ = RxCompassBuffer;
    i2cCompass.wRxQIndex = 0;
    i2cCompass.wRxQSize = 0;
    i2cCompass.bRxQReceiving = FALSE;

    TxCompassBuffer[0] = COMP_XYZ_DATA_CFG;
    TxCompassBuffer[1] = 0x01;
    i2cCompassWrite(&i2cCompass);
    __delay_cycles(20000);

    // write 0000 1101b = 0x0D to accelerometer control register 1
    // [7-6]: aslp_rate=00
    // [5-3]: dr=001 for 200Hz data rate (when in hybrid mode)
    // [2]: lnoise=1 for low noise mode
    // [1]: f_read=0 for normal 16 bit reads
    // [0]: active=1 to take the part out of standby and enable sampling
    i2cCompass.bySlaveAddress = COMPASS_ADDRESS_1;
    i2cCompass.ptrTxQ = TxCompassBuffer;
    i2cCompass.wTxQIndex = 0;
    i2cCompass.wTxQSize = 2;
    i2cCompass.bTxQSending = TRUE;
    i2cCompass.ptrRxQ = RxCompassBuffer;
    i2cCompass.wRxQIndex = 0;
    i2cCompass.wRxQSize = 0;
    i2cCompass.bRxQReceiving = FALSE;

    TxCompassBuffer[0] = COMP_CTRL_REG1_ADDR;
    TxCompassBuffer[1] = 0x0D;
    i2cCompassWrite(&i2cCompass);
    __delay_cycles(20000);

    return (WORD)COMP_CFG_OK;

}



// read status and the three channels of accelerometer and magnetometer data from
// FXOS8700CQ (13 bytes)
void AccelCompassReadStart(void)
{

    i2cCompass.bySlaveAddress = COMPASS_ADDRESS_1;
    i2cCompass.ptrTxQ = TxCompassBuffer;
    i2cCompass.wTxQIndex = 0;
    i2cCompass.wTxQSize = 1;
    i2cCompass.bTxQSending = TRUE;
    i2cCompass.ptrRxQ = RxCompassBuffer;
    i2cCompass.wRxQIndex = 0;
    i2cCompass.wRxQSize = 13;
    i2cCompass.bRxQReceiving = TRUE;

    TxCompassBuffer[0] = COMP_STATUS;
//    SetOutputPin(TP2, TRUE);
    i2cCompassRead(&i2cCompass);

}



// copy the compass data into the given buffer
void AccelCompassGetRawData(BYTE *pData, WORD maxSize)
{
    unsigned int i=0;
    while((i< 13)&&(i<maxSize))
    {
        pData[i] = RxCompassBuffer[i];
        i++;
    }
}



// CTRL_REG3 bit descriptions
//bit7:     fifo_gate ='0'
//          0: FIFO gate is bypassed. FIFO is flushed upon the system mode transitioning from Wake-to-Sleep mode or from Sleep to Wake mode.
//          1: The FIFO input buffer is blocked when transitioning from �Wake-to-Sleep� mode or from �Sleep-to-Wake� mode until the FIFO is flushed.
//bit6:     wake_tran ='0'
//          0: Transient function is disabled in Sleep mode
//          1: Transient function is enabled in Sleep mode and can generate an interrupt to wake the system
//bit5:     wake_lndprt ='0'
//          0: Orientation function is disabled Sleep mode.
//          1: Orientation function is enabled in Sleep mode and can generate an interrupt to wake the system
//bit4:     wake_pulse ='0'
//          0: Pulse function is disabled in Sleep mode
//          1: Pulse function is enabled in Sleep mode and can generate an interrupt to wake the system
//bit3:     wake_ffmt = '0'
//          0: Freefall/motion function is disabled in Sleep mode
//          1: Freefall/motion function is enabled in Sleep mode and can generate an interrupt to wake the system
//bit2:     wake_en_a_vecm = '0'
//          0: Acceleration vector-magnitude function is disabled in Sleep mode
//          1: Acceleration vector-magnitude function is enabled in Sleep mode and can generate an interrupt to wake the system
//bit1:     ipol = '1'
//          The ipol The bit selects the logic polarity of the interrupt signals output on the INT1 and INT2 pins.
//          INT1/INT2 interrupt logic polarity:
//          0: Active low (default)
//          1: Active high
//bit0:     pp_od  = '0'
//          INT1/INT2 push-pull or open-drain output mode selection. The open-drain configuration can be used for connecting
//          multiple interrupt signals on the same interrupt line but will require an external pullup resistor to function correctly.
//          0: Push-pull (default)
//          1: Open-drain
//
// CTRL_REG3  =  0000 0010b = 0x02


//CTRL_REG4 [Interrupt Enable Register] (0x2D) register
//bit7:     int_en_aslp -  Sleep interrupt enable
//          0: Auto-Sleep/Wake interrupt disabled
//          1: Auto-Sleep/Wake interrupt enabled
//bit6:     int_en_fifo -   FIFO interrupt enable
//          0: FIFO interrupt disabled
//          1: FIFO interrupt enabled
//bit 5:    int_en_trans - Transient interrupt enable
//          0: Transient interrupt disabled
//          1: Transient interrupt enabled
//bit 4:    int_en_lndprt -  Orientation interrupt enable
//          0: Orientation (Landscape/Portrait) interrupt disabled
//          1: Orientation (Landscape/Portrait) interrupt enabled
//bit 3:    int_en_pulse - Pulse interrupt enable
//          0: Pulse detection interrupt disabled
//          1: Pulse detection interrupt enabled
//bit 2:    int_en_ffmt - Freefall/motion interrupt enable
//          0: Freefall/motion interrupt disabled
//          1: Freefall/motion interrupt enabled
//bit 1:    int_en_a_vecm -  Acceleration vector-magnitude interrupt enable
//          0: Acceleration vector-magnitude interrupt disabled
//          1: Acceleration vector-magnitude interrupt enabled
//bit 0:    int_en_drdy -  Data-ready interrupt enable
//          0: Data-ready interrupt disabled
//          1: Data-ready interrupt enabled
//  CTRL_REG4  =  0000 0010b = 0x02


//CTRL_REG5 [Interrupt Routing Configuration Register] (0x2E) register
//bit7:     int_cfg_aslp - Sleep interrupt routing
//          0: Interrupt is routed to INT2 pin
//          1: Interrupt is routed to INT1 pin
//bit 6:     int_cfg_fifo - FIFO interrupt routing
//          0: Interrupt is routed to INT2 pin
//          1: Interrupt is routed to INT1 pin
//bit 5:    int_cfg_trans - Transient detection interrupt routing
//          0: Interrupt is routed to INT2 pin
//          1: Interrupt is routed to INT1 pin
//bit 4:    int_cfg_lndprt - Orientation detection interrupt routing
//          0: Interrupt is routed to INT2 pin
//          1: Interrupt is routed to INT1 pin
//bit 3:    int_cfg_pulse -   Pulse detection interrupt routing
//          0: Interrupt is routed to INT2 pin
//          1: Interrupt is routed to INT1 pin
//bit 2:    int_cfg_ffmt -  Freefall/motion detection interrupt routing
//          0: Interrupt is routed to INT2 pin
//          1: Interrupt is routed to INT1 pin
//bit 1:    int_cfg_a_vecm - Acceleration vector-magnitude interrupt routing
//          0: Interrupt is routed to INT2 pin
//          1: Interrupt is routed to INT1 pin.
//bit 0:    int_cfg_drdy - INT1/INT2 configuration.
//          0: Interrupt is routed to INT2 pin
//          1: Interrupt is routed to INT1 pin.
//
//  CTRL_REG5  =  0000 0010b = 0x02




