//******************************************************************************
//
//  BatteryGasI2C.h: Module Title
//
//      Copyright (c) 2010, Microlynx Systems Ltd
//      ALL RIGHTS RESERVED
//
//  This module contains the code to read and write the I2C CODEC.
//
//  Date            Author      Comment
//  ----------------------------------------------------------------
//  2011-Nov-10     CT          Adapted from EEPROM_I2C
//  2012-Mar-09     KM          Review / Update
//  2012-Mar-13     CT          Ported from Codec_I2C
//
//*******************************************************************************

#ifndef _BATTERYGASI2C_H
#define _BATTERYGASI2C_H

//------------------------------------------------------------------------------
//  INCLUDES
//------------------------------------------------------------------------------
#include "..\drivers.h"


//------------------------------------------------------------------------------
//  CONSTANT & MACRO DEFINITIONS
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
//  TYPEDEF DECLARATIONS
//------------------------------------------------------------------------------



//------------------------------------------------------------------------------
//  PUBLIC FUNCTION PROTOTYPES
//------------------------------------------------------------------------------

//******************************************************************************
//
//  Function: STC3100I2cInit
//
//  Arguments: void
//
//  Returns: TRUE if successful, else FALSE
//
//  Description: Initialize the I2C Coulumb counter.
//
//******************************************************************************
//BOOL STC3100I2cInit( void );


//******************************************************************************
//
//  Function: STC3100I2cBusy
//
//  Arguments: none
//
//  Returns: TRUE if CODEC is busy (internal write), else FALSE if ready for new write
//
//  Description: Checks if Coulumb counter is busy with internal write.
//
//******************************************************************************
BOOL STC3100I2cBusy( void );


//******************************************************************************
//
//  Function: STC3100WriteReg
//
//  Arguments: regAddr - register address
//             regVal - write data
//
//  Returns: TRUE if all data was written, else FALSE
//
//  Description: Writes data to the Coulumb counter.
//
//******************************************************************************
BOOL STC3100WriteRegs (BYTE regAddr, BYTE *pRegVal , BYTE length);


//******************************************************************************
//
//  Function: STC3100I2cRead
//
//  Arguments: Address - register address to start read from
//             Length - number of bytes to read
//
//  Returns: TRUE if requested length was read, else FALSE
//
//  Description: Reads requested number of bytes from the Coulomb counter.
//
//******************************************************************************
BOOL STC3100I2cRead( BYTE regAddr, WORD Length );


BOOL STC3100DataReady(void);


BOOL STC3100TxDone(void);

void STC3100GetRawData(BYTE *pData, WORD maxSize);


#endif
