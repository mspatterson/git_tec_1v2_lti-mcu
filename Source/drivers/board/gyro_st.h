#ifndef GYRO_ST_H_
#define GYRO_ST_H_


#include "..\drivers.h"


#define MAX_ST_OUT_SIZE	25
#define MAX_ST_IN_SIZE  25

#define ST_WAKEUP_PERIOD	500

#define ST_PWR		0x01		// P1.7 had damage so we are using P3.0. A high applies power to the L3G4200D
#define ST_CSn		0x04		// P2.2 (not per the schematic)
#define ST_AVG_BUFF_SIZE	10	// number of values that are averaged for avgSTz
#define ST_READ			0x80	// MS bit is high in addresss if read is requested
#define ST_MULTI_ADS	0x40	// Will auto increment the address if high
#define GET_TEMP_CMD	0x26
#define SET_REG1		0x20
#define SET_REG3		0X22
#define SET_REG4		0X23
#define SET_REG5		0X24
#define FIFO_ENABLE		0X40
#define OUT_Z_L			0x2C	// Can be read in a continuous sequence
#define OUT_Z_H			0x2D
#define SET_FIFO		0x2E
#define SET_SELF_TEST	0X02
#define SET_SELF_TEST_2K	0X32	// SELF TEST AT 2000dps
#define SET_2K_DPS		0X30
#define SET_BDU			0X80
#define ID_REG			0x0F
#define FILLER			0x00	// Needed to shift extra bytes out during a read
#define ST_GYRO_TOTAL	1


//extern unsigned char SToutBuff[MAX_ST_OUT_SIZE];
//extern unsigned char SToutPtr;
//extern unsigned char STtxSize;

//extern unsigned char STinBuff[MAX_ST_IN_SIZE];
//extern unsigned char STinPtr;

void initST(void);
void readSTtemp(void);
void setSTreg1(void);
void setSTreg3(void);
void getSTz(void);
void dispSTz(void);
void analyzeSTz(void);
void ST4200Send(unsigned char * data, unsigned char size);
void calibrateStGyro(void);
void readSTid(void);
char gedStId(void);
void setSTfifo(void);
BOOL stGyroHasData(void);
BOOL stGyroReadData( int *pResult);
//BOOL stGyroReadData( int *pResultRPM,int *pResultDPS, BYTE gNumber);
void setSTSelfTest(void);
void SelfTestStGyro(void);
void setSTNormalMode(void);
void setSTreg3NormalMode(void);
void SetSTGyroInterrupt(void);


void GetSTGyroData(void);
//WORD GetRPM(void);
BYTE GetRPM(void);
void PowerDownStGyro(void);
void WakeUpStGyro(void);

#endif /*GYRO_ST_H_*/
