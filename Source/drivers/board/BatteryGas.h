/*
 * BatteryGas.h
 *
 *  Created on: 2012-03-14
 *      Author: Engineer
 */

#ifndef BATTERYGAS_H_
#define BATTERYGAS_H_

#include "..\drivers.h"


void InitBatteryGasMonitor( void );
   // initialize the coulomb counter that measures battery charge - the 'gas'
   // gauge for the battery


//BYTE UpdateBatteryGasMonitor( BYTE* ptrNewChargeLevel );
    // this process monitors battery charge level and reports back a percentage
    // of total battery charge remaining in battery; i.e., it reports 89% when
    // there is 89% of battery charge left


//void updateBattUnderVoltageMonitor( BOOL* ptrBattUVLOIsValid, BOOL* ptrBattOk );
    // this process monitors undervoltage condition on the battery; it should
    // be called every 5 seconds; if '*ptrBattUVLOIsValid' is true, then
    // '*ptrBattOk' indicate 'TRUE' if battery is OK, and 'FALSE' if the
    // 'filtered' battery voltage has gone undervoltage long enough to indicate
    // the system should be shut down from the battery.


//void ResetBattUnderVoltageMonitor( void );
    // resets the battery undervoltage monitor process described in 'updateBattUnderVoltageMonitor'


void BatteryGasMonitorReadAllRegs(void);

void BatteryGasMonitorReadId(void);

void ResetBatteryGasMonitor( void );

void PowerDownBatteryGasMonitor( void );

BYTE GetBatLife(void);

void ParseGasGaugeData(BYTE *pData);

WORD GetBatUsedmAh(void);

WORD GetBatVoltageGasGauge(void);

#endif /* BATTERYGAS_H_ */
