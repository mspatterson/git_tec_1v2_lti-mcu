#ifndef RTC_H_
#define RTC_H_

#include "..\drivers.h"

#define RTC_ADDRESS		0x68


void StopRtc(void);
BOOL RtcTxDone(void);
BOOL RtcDataReady(void);


#endif /*RTC_H_*/
