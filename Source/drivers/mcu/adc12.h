/*
 * adc12.h
 *
 *  Created on: 2013-04-08
 *      Author: Owner
 */

#ifndef ADC12_H_
#define ADC12_H_

#include "..\drivers.h"


#define ADC12_CHANNELS	3
#define ADC12_2V5_REF_COEF_UV		610			//	2v5 reference / 4095 * 1000000
#define ADC12_COEFF_DIV10			ADC12_2V5_REF_COEF_UV/10


void Adc12Init(void);
void Adc12StartConversion(void);
BOOL IsAdc12ConversionDone(void);
void Adc12TakeSample(void);
void Adc12Disable(void);
void GetAdc12Results(WORD * pWord, WORD maxSize);
void MeasureBatVoltageAdc(void);
WORD GetBatVoltageAdc(void);
#endif /* ADC12_H_ */
